##Version works locally
"""
import json
import boto3
from zipfile import ZipFile
import mimetypes
from io import BytesIO

s3 = boto3.resource('s3')

session = boto3.Session(profile_name='python')
s3 = session.resource('s3')

client = boto3.client('s3')

portfolio_bucket = s3.Bucket('portfolio.allerium.net')
build_bucket = s3.Bucket('portfolio.allerium.net-build')

build_bucket.download_file('portfoliobuild.zip', '/tmp/portfoliobuild.zip')

with ZipFile("/tmp/portfoliobuild.zip") as myzip:
    for file in myzip.namelist():
        portfolio_bucket.upload_file(file, file,
            ExtraArgs={'ContentType': mimetypes.guess_type(file)[0]})
        client.put_object_acl(ACL='public-read', Bucket='portfolio.allerium.net', Key=file)

"""
###Version Used by lambda

import boto3
from zipfile import ZipFile
import mimetypes
from io import BytesIO

def lambda_handler(event, context):
    s3 = boto3.resource('s3')
    client = boto3.client('s3')
    sns = boto3.resource('sns')
    topic = sns.Topic('arn:aws:sns:us-east-1:914344209497:PortfolioDeploymen')
    topic.load()

    location = {
        "bucketName": "portfolio.allerium.net-build",
        "objectKey": "portfoliobuild.zip"
    }

    try:
        job = event.get("CodePipeline.job")

        if job:
            print("Job: " + str(job))
            for artifact in job["data"]["inputArtifacts"]:
                if artifact["name"] == "BuildArtifact":
                    location = artifact["location"]["s3Location"]

        print("Building from " + str(location))

        portfolio_bucket = s3.Bucket('portfolio.allerium.net')
        build_bucket = s3.Bucket(location["bucketName"])

        portfolio_zip = BytesIO()
        build_bucket.download_fileobj(location["objectKey"], portfolio_zip)

        with ZipFile(portfolio_zip) as myzip:
            for file in myzip.namelist():
                obj = myzip.open(file)
                portfolio_bucket.upload_fileobj(obj, file,
                    ExtraArgs={'ContentType': mimetypes.guess_type(file)[0]})
                client.put_object_acl(ACL='public-read', Bucket='portfolio.allerium.net', Key=file)

        print("Felicitations! C'est tout")
        topic.publish(Subject="Portfolio Deployment SUCCESSFUL", Message="Your portfolio update deployed successfully.")
        if job:
            codepipeline = boto3.client('codepipeline')
            codepipeline.put_job_success_result(jobId=job["id"])
    except:
        topic.publish(Subject="Portfolio Deployment FAILED", Message="Your portfolio update failed to deploy.")
        raise

    return
